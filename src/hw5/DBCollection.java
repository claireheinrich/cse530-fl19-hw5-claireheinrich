package hw5;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.UUID;
import java.util.Map.Entry;

import hw5.Document;

import com.google.gson.JsonObject;

public class DBCollection {

	private String collectionName;
	private File collection;

	/**
	 * Constructs a collection for the given database
	 * with the given name. If that collection doesn't exist
	 * it will be created.
	 */
	public DBCollection(DB database, String name) {
		this.collectionName = name;
		File newFile = new File("testfiles/" + database.getDatabaseName() + "/" + name + ".txt");
		if (!newFile.exists()) {
			try {
				newFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		this.collection = newFile;
	}

	public File getDBCollectionFile() {
		return this.collection;
	}

	/**
	 * Returns a cursor for all of the documents in
	 * this collection.
	 */
	public DBCursor find() {
		return new DBCursor(this, null, null);
	}

	/**
	 * Finds documents that match the given query parameters.
	 * 
	 * @param query relational select
	 * @return
	 */
	public DBCursor find(JsonObject query) {
		return new DBCursor(this, query, null);
	}

	/**
	 * Finds documents that match the given query parameters.
	 * 
	 * @param query relational select
	 * @param projection relational project
	 * @return
	 */
	public DBCursor find(JsonObject query, JsonObject projection) {
		return new DBCursor(this, query, projection);
	}

	/**
	 * Inserts documents into the collection
	 * Must create and set a proper id before insertion
	 * When this method is completed, the documents
	 * should be permanently stored on disk.
	 * @param documents
	 */
	public void insert(JsonObject... documents) {
		try {
			RandomAccessFile raf = new RandomAccessFile(this.collection, "rw");
			for (int i = 0; i < documents.length; i++) {
				if (documents[i] == null) {
					System.out.println("Please make sure your json array is full before inserting");
					return;
				}
				UUID uuid = UUID.randomUUID();
				documents[i].addProperty("_id", uuid.toString());
				String str = Document.toJsonString(documents[i]);
				byte[] document = str.getBytes();
				// make a header for each doc with a 1 for active, 0 for removed, followed by int of length
				byte[] header = new byte[5];
				header[0] = (byte) 1;
				byte[] len = ByteBuffer.allocate(4).putInt(document.length).array();
				for (int j = 1; j < header.length; j++) {
					header[j] = len[j - 1];
				}
				raf.seek(raf.length());
				raf.write(header);
				raf.write(document);
			}
			raf.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Locates one or more documents and replaces them
	 * with the update document.
	 * @param query relational select for documents to be updated
	 * @param update the document to be used for the update
	 * @param multi true if all matching documents should be updated
	 * 				false if only the first matching document should be updated
	 */
	public void update(JsonObject query, JsonObject update, boolean multi) {
		DBCursor results = new DBCursor(this, query, null);
		remove(query, multi);
		JsonObject[] insertData;
		try {
			RandomAccessFile raf = new RandomAccessFile(this.collection, "rw");
			if (!multi) {
				insertData = new JsonObject[1];
				if (results.hasNext()) {
					JsonObject doc = results.next().getKey();
					insertData[0] = doc;
				}
			} else {
				insertData = new JsonObject[(int) results.count()];
				int counter = 0;
				while (results.hasNext()) {
					JsonObject doc = results.next().getKey();
					insertData[counter] = doc;
					counter++;
				}
			}
			insert(insertData);
			raf.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Removes one or more documents that match the given
	 * query parameters
	 * @param query relational select for documents to be removed
	 * @param multi true if all matching documents should be updated
	 * 				false if only the first matching document should be updated
	 */
	public void remove(JsonObject query, boolean multi) {
		DBCursor results = new DBCursor(this, query, null);
		try {
			RandomAccessFile raf = new RandomAccessFile(this.collection, "rw");
			if (!multi) {
				if (results.hasNext()) {
					int index = results.next().getValue();
					byte[] header = getHeader(index);
					byte[] newHeader = new byte[5];
					byte[] toInt = new byte[4];
					newHeader[0] = (byte) 0;
					for (int i = 1; i < header.length; i++) {
						if (i < newHeader.length) {
							newHeader[i] = header[i];
						} else {
							toInt[i - 5] = header[i];
						}
					}
					int totalSkip = ByteBuffer.wrap(toInt).getInt();
					raf.seek(totalSkip);
					raf.write(newHeader);
				}
			} else {
				while (results.hasNext()) {
					int index = results.next().getValue();
					byte[] header = getHeader(index);
					byte[] newHeader = new byte[5];
					byte[] toInt = new byte[4];
					newHeader[0] = (byte) 0;
					for (int i = 1; i < header.length; i++) {
						if (i < newHeader.length) {
							newHeader[i] = header[i];
						} else {
							toInt[i - 5] = header[i];
						}
					}
					int totalSkip = ByteBuffer.wrap(toInt).getInt();
					raf.seek(totalSkip);
					raf.write(newHeader);
				}
			}
			raf.close();
			return;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Returns the number of documents in this collection
	 */
	public long count() {
		try {
			RandomAccessFile raf = new RandomAccessFile(this.collection, "r");
			int counter = 0;
			int skip = 0;
			while (skip < raf.length()) {
				byte[] header = new byte[5];
				raf.seek(skip);
				raf.read(header);
				if (header[0] == (byte) 1) {
					counter++;
				}
				byte[] skipArray = new byte[4];
				for (int c = 1; c < header.length; c++) {
					skipArray[c - 1] = header[c];
				}
				skip = skip + ByteBuffer.wrap(skipArray).getInt() + 5;
			}
			raf.close();
			return counter;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public String getName() {
		return this.collectionName;
	}

	/**
	 * Returns the ith header in the collection
	 */
	public byte[] getHeader(int i) {
		byte[] answer = new byte[9];
		try {
			RandomAccessFile raf = new RandomAccessFile(this.collection, "r");
			int counter = 0;
			int skip = 0;
			byte[] header = new byte[5];
			while (counter <= i) {
				raf.seek(skip);
				raf.read(header);
				if (counter < i) {
					byte[] skipArray = new byte[4];
					for (int c = 1; c < header.length; c++) {
						skipArray[c - 1] = header[c];
					}
					skip = skip + ByteBuffer.wrap(skipArray).getInt() + 5;
				}
				if (header[0] == (byte) 1) {
					counter++;
				}
			}
			raf.close();
			byte[] totalSkipArray = ByteBuffer.allocate(4).putInt(skip).array();
			for (int c = 0; c < answer.length; c++) {
				if (c < header.length) {
					answer[c] = header[c];
				} else {
					answer[c] = totalSkipArray[c - 5];
				}
			}
			return answer;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return answer;
	}

	/**
	 * Returns the ith document in the collection.
	 * Documents are separated by a line that contains a header for each document
	 * Use the parse function from the document class to create the document object
	 */
	public JsonObject getDocument(int i) {
		try {
			RandomAccessFile raf = new RandomAccessFile(this.collection, "r");
			int counter = 0;
			int skip = 0;
			int documentLength = 0;
			while (counter <= i) {
				byte[] header = new byte[5];
				raf.seek(skip);
				raf.read(header);
				if (header[0] == (byte) 1) {
					counter++;
				}
				byte[] skipArray = new byte[4];
				for (int c = 1; c < header.length; c++) {
					skipArray[c - 1] = header[c];
				}
				skip = skip + ByteBuffer.wrap(skipArray).getInt() + 5;
				documentLength = ByteBuffer.wrap(skipArray).getInt();
			}
			byte[] contents = new byte[documentLength];
			raf.seek(skip - documentLength);
			raf.read(contents);
			raf.close();
			String docString = new String(contents);
			if (docString.isEmpty()) {
				return new JsonObject();
			}
			return Document.parse(docString);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new JsonObject();
	}

	/**
	 * Drops this collection, removing all of the documents it contains from the DB
	 */
	public void drop() {
		this.collection.delete();
	}

}
