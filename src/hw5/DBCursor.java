package hw5;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class DBCursor implements Iterator<Entry<JsonObject, Integer>> {

	private Map<JsonObject, Integer> results;
	private Iterator<Map.Entry<JsonObject, Integer>> resultsIt;

	public DBCursor(DBCollection collection, JsonObject query, JsonObject fields) {
		// make data structure to store matching json objects and position in collection
		this.results = new HashMap<JsonObject, Integer>();

		//handle the case where query = null
		if (query == null) {
			for (int i = 0; i < collection.count(); i++) {
				this.results.put(collection.getDocument(i), i);
			}
		} else {
			// check each doc in the collection to see if it matches the query
			for (int i = 0; i < collection.count(); i++) {
				Set<Map.Entry<String, JsonElement>> params = query.entrySet();
				Iterator<Map.Entry<String, JsonElement>> paramsIt = params.iterator();
				boolean isMatch = true;
				JsonObject document = collection.getDocument(i); //setup

				while (paramsIt.hasNext()) {
					// check each param in the doc to see if doc has it
					Entry<String, JsonElement> ent = paramsIt.next();
					if (document.has(ent.getKey())) {
						//check if doc has the key, does it match the query value
						if (!document.get(ent.getKey()).equals(ent.getValue())) {
							isMatch = false;
						}
					} else {
						isMatch = false;
					}
				}
				//add the doc to the results if it is a match
				if (isMatch) {
					if (fields != null) {
						Set<Map.Entry<String, JsonElement>> fieldsSet = fields.entrySet();
						Iterator<Map.Entry<String, JsonElement>> fieldsIt = fieldsSet.iterator();
						//make new JsonObjects out of the desired fields
						JsonObject editedDoc = new JsonObject();
						while (fieldsIt.hasNext()) {
							Entry<String, JsonElement> ent = fieldsIt.next();
							editedDoc.add(ent.getKey(), document.get(ent.getKey()));
						}
						this.results.put(editedDoc, i);
					} else {
						this.results.put(document, i);
					}
				}
			}
		}
		this.resultsIt = this.results.entrySet().iterator();
	}

	/**
	 * Returns true if there are more documents to be seen
	 */
	public boolean hasNext() {
		return this.resultsIt.hasNext();
	}

	/**
	 * Returns the next document
	 */
	public Entry<JsonObject, Integer> next() {
		return this.resultsIt.next();
	}

	/**
	 * Returns the total number of documents
	 */
	public long count() {
		return results.size();
	}

}
