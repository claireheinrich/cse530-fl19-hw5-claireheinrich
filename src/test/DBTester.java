package test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;

import org.junit.jupiter.api.Test;

import hw5.DB;
import hw5.DBCollection;

class DBTester {

	/**
	 * Things to consider testing:
	 * 
	 * Properly creates directory for new DB (done)
	 * Properly accesses existing directory for existing DB
	 * Properly accesses collection
	 * Properly drops a database
	 * Special character handling?
	 */

	@Test
	public void testCreateDB() {
		DB hw5 = new DB("hw5"); //call method
		assertTrue(new File("testfiles/hw5").exists()); //verify results
	}

	@Test
	public void testAccessDB() {
		DB hw5 = new DB("hw5"); //call method to be tested
		// how do I show this is true? that there is only one?
	}

	@Test
	public void testCreateCollection() {
		DB hw5 = new DB("hw5"); //setup
		DBCollection a = hw5.getCollection("a"); //call method to be tested
		assertTrue(new File("testfiles/hw5/a.txt").exists()); //verify results
	}

	@Test
	public void testAccessCollection() {
		DB hw5 = new DB("hw5"); //setup
		DBCollection a = hw5.getCollection("a"); //call method to be tested
		// how do I show this is true?
	}

	@Test
	public void testDropDB() {
		DB hw5 = new DB("hw5"); //setup
		hw5.dropDatabase(); //call method to be tested
		assertFalse(new File("testfiles/hw5").exists()); //verify results
	}

}
