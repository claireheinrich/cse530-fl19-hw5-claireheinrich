package test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;

import org.junit.jupiter.api.Test;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import hw5.DB;
import hw5.DBCollection;
import hw5.DBCursor;

class CollectionTester {

	/**
	 * Things to consider testing
	 * 
	 * Queries:
	 * 	Find all
	 * 	Find with relational select
	 * 		Conditional operators
	 * 		Embedded documents
	 * 		Arrays
	 * 	Find with relational project
	 * 
	 * Inserts
	 * Updates
	 * Deletes
	 * 
	 * getDocument (done?)
	 * drop
	 */

	@Test
	public void testGetDocument() {
		DB db = new DB("testdata");
		DBCollection test = db.getCollection("test2");
		JsonObject primitive = test.getDocument(2);
		assertTrue(primitive.getAsJsonPrimitive("key").getAsString().equals("hello, world"));
	}

	@Test
	public void testDrop() {
		DB db = new DB("testdata");
		DBCollection test1 = db.getCollection("test1");
		test1.drop();
		assertFalse(new File(db.getDatabaseFile(), "test1.txt").exists());
	}

	@Test
	public void testFindAll() {
		DB db = new DB("testdata");
		DBCollection test = db.getCollection("test2"); //get DB and collection
		DBCursor results = test.find(); //call method to be tested
		assertEquals(test.count(), results.count());
		assertEquals(test.getDocument(2), results.next());
		assertEquals(test.getDocument(4), results.next());
		assertEquals(test.getDocument(1), results.next());
		assertEquals(test.getDocument(0), results.next());
		assertEquals(test.getDocument(3), results.next());
	}

	@Test
	public void testFindConditionalEmbedded() {
		DB db = new DB("testdata");
		DBCollection test = db.getCollection("test2"); //get DB and collection
		JsonObject query = new JsonObject();
		JsonObject innerJson = new JsonObject();
		innerJson.addProperty("embeddedKey", "value");
		query.add("key", innerJson);

		DBCursor results = test.find(query); //call method to be tested
		assertEquals(1, results.count());
		assertEquals(test.getDocument(1), results.next());
		assertFalse(results.hasNext());
	}

	@Test
	public void testFindConditionalArray() {
		DB db = new DB("testdata");
		DBCollection test = db.getCollection("test2"); //get DB and collection
		JsonObject query = new JsonObject();
		JsonArray array1 = new JsonArray(3);
		array1.add("abc");
		array1.add("def");
		array1.add("ghi");
		query.add("key", array1);

		DBCursor results = test.find(query); //call method to be tested
		assertEquals(1, results.count());
		assertEquals(test.getDocument(0), results.next());
		assertFalse(results.hasNext());
	}

	@Test
	public void testFindWithProject() {
		DB db = new DB("testdata");
		DBCollection test = db.getCollection("test2"); //get DB and collection
		JsonObject query = new JsonObject();
		query.addProperty("key", "value");
		JsonObject project = new JsonObject();
		project.addProperty("key2", "1");

		DBCursor results = test.find(query, project); //call method to be tested
		assertEquals(1, results.count());
		//FIX THIS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		assertEquals(test.getDocument(4), results.next());
		assertFalse(results.hasNext());
	}

	@Test
	public void testInsertEmptyArray() {
		DB db = new DB("testdata");
		DBCollection test = db.getCollection("test2"); //get DB and collection
		JsonObject[] insertData = new JsonObject[3]; // make an empty array
		long docCount = test.count(); //get count of documents in collection before insert

		test.insert(insertData);//call method to be tested
		assertEquals(docCount, test.count());//this insert should have no impact on the document count
	}

	@Test
	public void testInsert() {
		// clear out the contents of test2.txt before inserting
		try {
			PrintWriter pw = new PrintWriter("testfiles/testdata/test2.txt");
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		DB db = new DB("testdata");
		DBCollection test = db.getCollection("test2"); //get DB and collection

		JsonObject[] insertData = new JsonObject[3];
		JsonObject json1 = new JsonObject();
		JsonArray array1 = new JsonArray(3);
		array1.add("abc");
		array1.add("def");
		array1.add("ghi");
		json1.add("key", array1);
		insertData[0] = json1;
		JsonObject json2 = new JsonObject();
		JsonObject innerJson = new JsonObject();
		innerJson.addProperty("embeddedKey", "value");
		json2.add("key", innerJson);
		insertData[1] = json2;
		JsonObject json3 = new JsonObject();
		json3.addProperty("key", "hello, world"); //setup 3 documents, add to array, add to this collection
		insertData[2] = json3;

		test.insert(insertData);//call method to be tested

		assertEquals(3, test.count());//there should be three elements in the collection
		assertEquals(json1, test.getDocument(0));
		assertEquals(json2, test.getDocument(1));
		assertEquals(json3, test.getDocument(2)); //verify contents of test collection
	}

	@Test
	public void testInsertDoesNotOverwrite() {
		// clear out the contents of test2.txt before inserting
		try {
			PrintWriter pw = new PrintWriter("testfiles/testdata/test2.txt");
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		testInsert();
		DB db = new DB("testdata");
		DBCollection test = db.getCollection("test2"); //get DB and collection

		JsonObject json1 = new JsonObject();
		JsonArray array1 = new JsonArray(3);
		array1.add("abc");
		array1.add("def");
		array1.add("ghi");
		json1.add("key", array1);
		JsonObject json2 = new JsonObject();
		JsonObject innerJson = new JsonObject();
		innerJson.addProperty("embeddedKey", "value");
		json2.add("key", innerJson);
		JsonObject json3 = new JsonObject();
		json3.addProperty("key", "hello, world"); //setup 3 documents that should already exist

		JsonObject[] insertData = new JsonObject[2];
		JsonObject json4 = new JsonObject();
		JsonObject innerJson4 = new JsonObject();
		JsonArray array2 = new JsonArray(2);
		array2.add("test");
		array2.add("insert");
		innerJson4.add("embeddedKey", array2);
		json4.add("key", innerJson4);
		insertData[0] = json4;
		JsonObject json5 = new JsonObject();
		json5.addProperty("key", "value");
		json5.addProperty("key2", "value2");
		insertData[1] = json5; //craft json objects to insert 

		test.insert(insertData);//call method to be tested

		assertEquals(5, test.count());//there should be three elements in the collection
		assertEquals(json4, test.getDocument(3));
		assertEquals(json5, test.getDocument(4)); //verify contents of test collection
	}

	@Test
	public void testUpdate() {
		// clear out the contents of test6.txt before inserting
		try {
			PrintWriter pw = new PrintWriter("testfiles/testdata/test5.txt");
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		DB db = new DB("testdata");
		DBCollection test = db.getCollection("test5"); //get DB and collection

		JsonObject[] insertData = new JsonObject[4];
		JsonObject json1 = new JsonObject();
		json1.addProperty("key", "value");
		insertData[0] = json1;
		JsonObject json2 = new JsonObject();
		json2.addProperty("key", "value");
		insertData[1] = json2;
		JsonObject json3 = new JsonObject();
		json3.addProperty("key2", "value");
		insertData[2] = json3;
		JsonObject json4 = new JsonObject();
		json4.addProperty("key2", "value");
		insertData[3] = json4; //setup
		test.insert(insertData);

		JsonObject query = new JsonObject();
		query.addProperty("key", "value");
		JsonObject update = new JsonObject();
		update.addProperty("key", "HELLO, WORLD");
		test.update(query, update, false); //call method to be tested

		assertEquals(4, test.count());
		assertEquals(update.get("key"), test.getDocument(0).get("key"));
		assertEquals(json2.get("key"), test.getDocument(1).get("key"));
		assertEquals(json3.get("key2"), test.getDocument(2).get("key2"));
		assertEquals(json4.get("key2"), test.getDocument(3).get("key2"));
	}

	@Test
	public void testUpdateWithMulti() {
		// clear out the contents of test5.txt before inserting
		try {
			PrintWriter pw = new PrintWriter("testfiles/testdata/test5.txt");
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		testUpdate();

		DB db = new DB("testdata");
		DBCollection test = db.getCollection("test5"); //get DB and collection

		JsonObject query = new JsonObject();
		query.addProperty("key2", "value");
		JsonObject update = new JsonObject();
		update.addProperty("key2", "test");
		test.update(query, update, true); //call method to be tested

		assertEquals(4, test.count());
		assertEquals(update.get("key2"), test.getDocument(2).remove("key2"));
		assertEquals(update.get("key2"), test.getDocument(3).remove("key2"));
	}

	@Test
	public void testRemoveDocument() {
		// clear out the contents of test6.txt before inserting
		try {
			PrintWriter pw = new PrintWriter("testfiles/testdata/test6.txt");
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		DB db = new DB("testdata");
		DBCollection test = db.getCollection("test6"); //get DB and collection

		JsonObject[] insertData = new JsonObject[4];
		JsonObject json1 = new JsonObject();
		json1.addProperty("key", "value");
		insertData[0] = json1;
		JsonObject json2 = new JsonObject();
		json2.addProperty("key", "value");
		insertData[1] = json2;
		JsonObject json3 = new JsonObject();
		json3.addProperty("key2", "value");
		insertData[2] = json3;
		JsonObject json4 = new JsonObject();
		json4.addProperty("key2", "value");
		insertData[3] = json4; //setup
		test.insert(insertData);

		JsonObject query = new JsonObject();
		query.addProperty("key", "value");
		test.remove(query, false); //call method to be tested

		assertEquals(3, test.count());
		assertEquals(json2.get("key"), test.getDocument(0).get("key"));
		assertEquals(json3.get("key2"), test.getDocument(1).get("key2"));
		assertEquals(json4.get("key2"), test.getDocument(2).get("key2"));

	}

	@Test
	public void testRemoveDocumentWithMulti() {
		// clear out the contents of test6.txt before inserting
		try {
			PrintWriter pw = new PrintWriter("testfiles/testdata/test6.txt");
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		testRemoveDocument();

		DB db = new DB("testdata");
		DBCollection test = db.getCollection("test6"); //get DB and collection

		JsonObject query = new JsonObject();
		query.addProperty("key2", "value");
		test.remove(query, true); //call method to be tested

		assertEquals(1, test.count());
		assertEquals("value", test.getDocument(0).get("key"));
	}

}
