package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import hw5.Document;

class DocumentTester {

	/*
	 * Things to consider testing:
	 * 
	 * Invalid JSON
	 * 
	 * Properly parses embedded documents
	 * Properly parses arrays
	 * Properly parses primitives (done!)
	 * 
	 * Object to embedded document
	 * Object to array
	 * Object to primitive
	 */

	@Test
	public void testParsePrimitive() {
		String json = "{ \"key\":\"value\" }";//setup
		JsonObject results = Document.parse(json); //call method to be tested
		assertTrue(results.getAsJsonPrimitive("key").getAsString().equals("value")); //verify results
	}

	@Test
	public void testParseArray() {
		String json = "{ \"key\":[\"abc\",\"def\",\"ghi\"]}"; //setup
		JsonObject results = Document.parse(json); //call method to be tested
		assertTrue(results.getAsJsonArray("key").get(0).getAsString().equals("abc")); //verify results at 0
		assertTrue(results.getAsJsonArray("key").get(1).getAsString().equals("def")); //verify results at 1
		assertTrue(results.getAsJsonArray("key").get(2).getAsString().equals("ghi")); //verify results at 2
	}

	@Test
	public void testParseEmbeddedDocs() {
		String json = "{ \"key\":{ \"embeddedKey\":\"value\" }}"; //setup
		JsonObject results = Document.parse(json); //call method to be tested
		assertTrue(results.getAsJsonObject("key").getAsJsonPrimitive("embeddedKey").getAsString().equals("value")); //verify results
	}

	@Test
	public void testParseArrayofArrays() {
		String json = "{ \"key\":[[\"abc\",\"def\"],[\"ghi\"]] }"; //setup
		JsonObject results = Document.parse(json); //call method to be tested
		assertTrue(results.getAsJsonArray("key").get(0).getAsJsonArray().get(0).getAsString().equals("abc")); //verify results at 0,0
		assertTrue(results.getAsJsonArray("key").get(0).getAsJsonArray().get(1).getAsString().equals("def")); //verify results at 0,1
		assertTrue(results.getAsJsonArray("key").get(1).getAsJsonArray().get(0).getAsString().equals("ghi")); //verify results at 1,0
	}

	@Test
	public void testParseEmbeddedDocWithArray() {
		String json = "{ \"key\":{ \"embeddedKey\":[\"abc\",\"def\",\"ghi\"] }}"; //setup
		JsonObject results = Document.parse(json); //call method to be tested
		assertTrue(results.getAsJsonObject("key").getAsJsonArray("embeddedKey").get(0).getAsString().equals("abc")); //verify results at 0
		assertTrue(results.getAsJsonObject("key").getAsJsonArray("embeddedKey").get(1).getAsString().equals("def")); //verify results at 1
		assertTrue(results.getAsJsonObject("key").getAsJsonArray("embeddedKey").get(2).getAsString().equals("ghi")); //verify results at 2
	}

	@Test
	public void testJSONtoPrimitive() {
		JsonObject json = new JsonObject();
		json.addProperty("key", "value");
		String expected = "{\"key\":\"value\"}"; //setup
		assertEquals(expected, Document.toJsonString(json)); //call method to be tested and verify
	}

	@Test
	public void testJSONtoArray() {
		JsonObject json = new JsonObject();
		JsonArray array = new JsonArray(3);
		array.add("abc");
		array.add("def");
		array.add("ghi");
		json.add("key", array);
		String expected = "{\"key\":[\"abc\",\"def\",\"ghi\"]}"; //setup
		assertEquals(expected, Document.toJsonString(json)); //call method to be tested and verify
	}

	@Test
	public void testJSONtoEmbeddedDoc() {
		JsonObject json = new JsonObject();
		JsonObject innerJson = new JsonObject();
		innerJson.addProperty("embeddedKey", "value");
		json.add("key", innerJson);
		String expected = "{\"key\":{\"embeddedKey\":\"value\"}}"; //setup
		assertEquals(expected, Document.toJsonString(json)); //call method to be tested and verify
	}

}
