package test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

import org.junit.jupiter.api.Test;

import com.google.gson.JsonObject;

import hw5.DB;
import hw5.DBCollection;
import hw5.DBCursor;

class CursorTester {

	/**
	 * Things to consider testing:
	 * 
	 * hasNext (done?)
	 * count (done?)
	 * next (done?)
	 */

	@Test
	public void testFindAll() {
		// clear out the contents of test2.txt before inserting
		try {
			PrintWriter pw = new PrintWriter("testfiles/testdata/test4.txt");
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		DB db = new DB("testdata");
		DBCollection test = db.getCollection("test4");

		//insert 3 json objects to test dbcursor
		JsonObject[] insertData = new JsonObject[3];
		JsonObject json1 = new JsonObject();
		json1.addProperty("key", "1");
		insertData[0] = json1;
		JsonObject json2 = new JsonObject();
		json2.addProperty("key", "2");
		insertData[1] = json2;
		JsonObject json3 = new JsonObject();
		json3.addProperty("key", "3");
		insertData[2] = json3;
		test.insert(insertData);

		DBCursor results = test.find(); //call method to be tested

		assertTrue(results.count() == 3);
		assertTrue(results.hasNext());
		JsonObject d1 = results.next().getKey(); //pull first document
		//verify contents
		assertTrue(results.hasNext());//still more documents
		JsonObject d2 = results.next().getKey(); //pull second document
		//verfiy contents?
		assertTrue(results.hasNext()); //still one more document
		JsonObject d3 = results.next().getKey();//pull last document
		//verify contents?
		assertFalse(results.hasNext());//no more documents
	}
}
